#ifndef __PLAN_HPP__
#define __PLAN_HPP__

#include "globaldef.hpp"

/**
 * @brief 方案
 */
class Plan
{
private:
    bool validate = false; // 有效性

public:
    float value = 0; // 价值
    int berthID = INVALID_BERTH_ID; // 泊位ID

public:
    /**
     * @brief 方案是否有效
     * @return true 
     * @return false 
     */
    bool isValid() const {
        return this->validate;
    }
    /**
     * @brief 设置方案有效性
     * @param validate 
     */
    void setValidate(bool validate) {
        this->validate = validate;
    }
    /**
     * @brief 设置价值
     * @param value 
     */
    void setValue(float value) {
        this->value = value;
    }
    /**
     * @brief 重载小于运算
     * @param other 
     * @return true 
     * @return false 
     */
    bool operator<(const Plan &other) const {
        return value < other.value;
    }
};

#endif // __PLAN_HPP__