#ifndef __BLOCK_HPP__
#define __BLOCK_HPP__

#include "globaldef.hpp"
#include "Coord.hpp"

/**
 * @brief 区域
 */
class Block: public Coord
{
public:
    int x_width; // 横向宽度
    int y_width; // 纵向宽度

public:
    /**
     * @brief 构造函数
     * @param x 
     * @param y 
     * @param x_width 
     * @param y_width 
     */
    explicit Block(int x = -1, int y = -1, int x_width = 0, int y_width = 0): 
        Coord(x, y), 
        x_width(x_width),
        y_width(y_width) {

    }
};

#endif // __BLOCK_HPP__