#include <cstdio>
#include "globaldef.hpp"
#include "Solution.hpp"

int main(int argc, char **argv) {
    /* 变量定义 *//////////////////////////////////////////////////////////////////////////////////////////////////
    Solution s;

    /* LOG初始化 */////////////////////////////////////////////////////////////////////////////////////////////////
    #ifdef USE_LOG
    LOG_INIT(argc > 1 ? argv[1] : "log/log.txt"); // 判题器仅需阅读标准输出，将标准错误重定向到log.txt用于LOG输出
    #endif

    /* 场景初始化信息录入 *//////////////////////////////////////////////////////////////////////////////////////////
    // #define AUTO_MAKE_MAP_TABLE // 自动生成地图特征表
    #ifdef AUTO_MAKE_MAP_TABLE
    LOG("{")
    #endif
    for (int x = 0; x < MAP_X_MAX; ++x) {
        for (int y = 0; y < MAP_X_MAX; ++y) {
            char mark;
            scanf("%c", &mark);
            s.map.set(x, y, mark);
            #ifdef USE_LOG
            #ifdef AUTO_MAKE_MAP_TABLE
            if (mark == MARK_ROBOT) {
                LOG("_MT(%3d,%3d,'%c'),", x, y, mark);
            }
            #endif
            #endif
        }
        getchar();
    }
    #ifdef AUTO_MAKE_MAP_TABLE
    LOG("}\n")
    LOG("=======================================\n");
    #endif
    for (int i = 0; i < BERTH_NUM; ++i) {
        int id, x, y, distance, velocity;
        scanf("%d %d %d %d %d", &id, &x, &y, &distance, &velocity);
        s.berths[i].set(id, x, y, distance, velocity);
    }
    int boatVolume;
    scanf("%d", &boatVolume);
    for (int i = 0; i < BOAT_NUM; ++i) {
        s.boats[i].set(i, boatVolume);
    }
    for (int i = 0; i < ROBOT_NUM; ++i) {
        s.robots[i].set(i);
    }
    s.scanOK();
    s.init();
    s.printOK();
    LOG("=======================================\n");
    LOG("轮船容量[%d]\n", boatVolume);
    LOG("=======================================\n");

    /* 帧交互 *////////////////////////////////////////////////////////////////////////////////////////////////////
    int frameID; // 帧序号
    int money; // 当前金钱数
    #ifdef USE_LOG
    int lastFrameID = 0; // 上次循环的帧序号
    int initTimeoutFrames = 0; // 初始化未完成导致的跳帧次数
    int jumpFramesSum = 0; // 跳帧次数和
    int crashNum = 0; // 碰撞次数
    #endif

    while(std::scanf("%d %d\n", &frameID, &money) != EOF) {
        /* 跳帧检测 *///===========================================================================================
        #ifdef USE_LOG
        int jumpFrames = frameID - lastFrameID - 1;
        // if(jumpFrames > 0) {
        //     LOG("[%05d] 跳%02d帧 ----------------------\n", frameID, jumpFrames);
        // }
        if(lastFrameID == 0) {
            initTimeoutFrames = jumpFrames;
        } else {
            jumpFramesSum += jumpFrames;
        }
        lastFrameID = frameID;
        #endif

        /* 场面信息处理 *///========================================================================================
        // 输入货物信息
        int K; // 货物数量
        scanf("%d", &K);
        int invalidNum = 0;
        for (int i = 0; i < K; ++i) {
            int x, y, value;
            scanf("%d %d %d", &x, &y, &value);
            if(s.map.isBerthArrival(Coord(x, y))) { // 无效货物不推入列表
                s.goods_list.push_back(new Goods(x, y, frameID, value));
            } else {
                ++invalidNum;
            }
        }
        K -= invalidNum; // 去除无效货物

        // 输入机器人信息
        for (int i = 0; i < ROBOT_NUM; ++i) {
            int carried, x, y, running;
            scanf("%d %d %d %d", &carried, &x, &y, &running);
            s.robots[i].refresh(carried, x, y, running);

            #ifdef USE_LOG
            crashNum += running == 0 ? 1 : 0;
            #endif
        }

        // 输入船信息
        for (int i = 0; i < BOAT_NUM; ++i) {
            int state, berthID;
            scanf("%d %d", &state, &berthID);
            s.boats[i].refresh(state, berthID);
        }

        // 接收OK
        s.scanOK();

        // 处理与输出
        s.run(frameID, K);
        s.printOK();
    }

    /* LOG输出 *///////////////////////////////////////////////////////////////////////////////////////////////////
    #ifdef USE_LOG
    LOG("=======================================\n");
    bool abnormal = false;
    if (initTimeoutFrames > 0) {
        LOG("初始化阶段跳帧[%2d]\n", initTimeoutFrames);
        abnormal = true;
    }
    if (jumpFramesSum > 0) {
        LOG("帧交互阶段跳帧[%2d]\n", jumpFramesSum);
        abnormal = true;
    }
    if (crashNum > 0) {
        LOG("机器人碰撞次数[%2d]\n", crashNum / 20);
        abnormal = true;
    }
    if (!abnormal) {
        LOG("未出现异常情况！\n");
    }
    LOG("=======================================\n");
    #endif

    return 0;
}