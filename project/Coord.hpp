#ifndef __COORD_HPP__
#define __COORD_HPP__

#include "globaldef.hpp"

/**
 * @brief 坐标
 */
class Coord
{
public:
    int x; // 横坐标
    int y; // 纵坐标

public:
    /**
     * @brief 构造函数
     * @param x
     * @param y
     */
    explicit Coord(int x = -1, int y = -1):
        x(x), y(y) {

    }
    /**
     * @brief 构造函数
     * @param another
     * @param dir
     */
    Coord(const Coord &another, direction_t dir=INPLACE) {
        this->x = another.x;
        this->y = another.y;
        this->moveCoord(dir);
    }
    /**
     * @brief 判等运算符重载
     * @param other 
     * @return true 
     * @return false 
     */
    bool operator==(const Coord &other) const {
        return this->x == other.x && this->y == other.y;
    }
    /**
     * @brief 
     * @param other 
     * @return true 
     * @return false 
     */
    bool operator!=(const Coord &other) const {
        return !(*this == other);
    }
    /**
     * @brief 赋值运算符重载
     * @param other 
     * @return Coord 
     */
    const Coord &operator=(const Coord &other) {
        this->x = other.x;
        this->y = other.y;
        return *this;
    }
    /**
     * @brief 设置坐标
     * @param x
     * @param y
     */
    void setCoord(int x, int y) {
        this->x = x;
        this->y = y;
    }
    /**
     * @brief 判断坐标是否在矩形范围内（左上闭右下开区间）
     * @param x_min
     * @param y_min
     * @param x_max
     * @param y_max
     * @return true
     * @return false
     */
    bool isInRect(int x_min, int y_min, int x_max, int y_max) {
        if (this->x >= x_min && this->y >= y_min && this->x < x_max && this->y < y_max) {
            return true;
        }
        return false;
    }
    /**
     * @brief 向指定方向移动坐标
     * @param dir
     * @param reverse
     */
    void moveCoord(direction_t dir, bool reverse = false) {
        int step = reverse ? -1 : 1;
        switch (dir)
        {
        case UP:
            this->x -= step;
            break;

        case RIGHT:
            this->y += step;
            break;

        case DOWN:
            this->x += step;
            break;

        case LEFT:
            this->y -= step;
            break;

        default:
        case INPLACE:
            break;
        }
    }
};

#endif // __COORD_HPP__