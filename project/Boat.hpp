#ifndef __BOAT_HPP__
#define __BOAT_HPP__

#include "globaldef.hpp"
#include "Block.hpp"
#include <cstdio>
#include "Goods.hpp"
#include <list>

/**
 * @brief 船
 */
class Boat: public Block
{
public:
    int id; // 编号
    unsigned int volume; // 容积
    int state = -1; // 状态
    int berthID = -1; // 目标泊位号
    std::list<Goods *> goods_list; // 货物列表
    int leaveVirtualPointTime = 0; // 上次离开虚拟点的时间

public:
    /**
     * @brief 初始化
     * @param id 
     * @param volume 
     */
    void set(int id, unsigned int volume) {
        this->id = id;
        this->volume = volume;
    }
    /**
     * @brief 构造函数
     * @param id 
     * @param volume 
     */
    explicit Boat(int id = -1, unsigned int volume = -1):
        Block(-1, -1, BOAT_X_WIDTH, BOAT_Y_WIDTH) {
        set(id, volume);
    }
    /**
     * @brief 刷新状态
     * @param state 
     * @param berthID 
     */
    void refresh(int state, int berthID) {
        this->state = state;
        this->berthID = berthID;
    }
    /**
     * @brief 移动到指定泊位
     * @param berthID 
     */
    void move(int berthID) {
        if (berthID == VIRTUAL_BERTH_ID) {
            printf("go %d\n", id);
        } else {
            printf("ship %d %d\n", id, berthID);
        }
        std::fflush(stdout); // 标准输出需要及时刷新
        state = BOAT_MOVING; // 船离港的指令会立即生效
    }
    /**
     * @brief 是否已满载
     * @return true 
     * @return false 
     */
    bool isFull() const {
        return goods_list.size() >= volume;
    }
    /**
     * @brief 获取货物总价值
     */
    int getValue() const {
        int value = 0;
        for (auto goods : goods_list) {
            value += goods->value;
        }
        return value;
    }
    /**
     * @brief 获取当前已放置货物总数
     * @return int 
     */
    int getGoodsNum() const {
        return goods_list.size();
    }
    /**
     * @brief 获取剩余容量
     * @return int 
     */
    int getRemainVolume() const {
        return volume - goods_list.size();
    }
};

#endif // __BOAT_HPP__