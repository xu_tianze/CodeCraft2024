#ifndef __GOODS_HPP__
#define __GOODS_HPP__

#include "globaldef.hpp"
#include "Coord.hpp"
#include "Map.hpp"

/**
 * @brief 货物
 */
class Goods: public Coord
{
public:
    int value = 0; // 价值
    int endFrame = 0; // 消失时间
    bool isCarried = false; // 正在被携带
    Map::distable_t *distance = nullptr; // 距离表
    static int amount; // 对象分配计数

public:
    /**
     * @brief 构造函数
     * @param x 
     * @param y 
     * @param frameID 
     * @param value 
     */
    Goods(int x, int y, int frameID, int value):
        Coord(x, y),
        value(value),
        endFrame(frameID + GOODS_LIFE_FRAMES - 1) {
        ++amount;
    }
    /**
     * @brief 析构函数
     */
    ~Goods() {
        setUnused();
        --amount;
    }
    /**
     * @brief 货物已被携带
     */
    void setCarried() {
        isCarried = true;
    }
    /**
     * @brief 是否可获取
     * @param frameID
     * @return true 
     * @return false 
     */
    bool isAvailable(int frameID) const {
        return !isCarried && frameID <= endFrame;
    }
    /**
     * @brief 货物剩余寿命时间
     * @param frameID 
     * @return int 
     */
    int remainLifeTime(int frameID) const {
        int remainLife = endFrame - frameID;
        return remainLife < 0 ? 0 : remainLife;
    }
    /**
     * @brief 货物不再使用，回收距离表内存
     */
    void setUnused() {
        if (distance != nullptr) {
            delete distance;
            distance = nullptr;
        }
    }
};

int Goods::amount = 0;

#endif // __GOODS_HPP__