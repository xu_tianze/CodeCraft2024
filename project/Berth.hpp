#ifndef __BERTH_HPP__
#define __BERTH_HPP__

#include "globaldef.hpp"
#include "Block.hpp"
#include "Goods.hpp"
#include <deque>

/**
 * @brief 泊位
 */
class Berth: public Block
{
public:
    int id; // 编号
    int distance; // 该泊位轮船运输到虚拟点的时间
    int velocity; // 装载速度
    int loadTime = 0; // 将船容积装满需要的时间
    int boatVolume = 0; // 船的容积
    std::deque<Goods *> goods_list; // 货物列表
    bool locked = false; // 泊位是否被禁用

public:
    /**
     * @brief 初始化
     * @param id 
     * @param x 
     * @param y 
     * @param distance 
     * @param velocity 
     */
    void set(int id, int x, int y, int distance, int velocity) {
        this->id = id;
        this->x = x;
        this->y = y;
        this->distance = distance;
        this->velocity = velocity;
    }
    /**
     * @brief 设置装载时间
     * @param boatVolume 
     */
    void setLoadTime(int boatVolume) {
        this->boatVolume = boatVolume;
        this->loadTime = boatVolume / velocity + ((boatVolume % velocity) ? 1 : 0);
    }
    /**
     * @brief 获取船抵达并完成装货的时间
     * @return int 
     */
    int getBoatTime() const {
        return distance + loadTime;
    }
    /**
     * @brief 货物能装满几艘船
     * @return int 
     */
    int getFullBoatNum() const {
        return goods_list.size() / boatVolume;
    }
    /**
     * @brief 构造函数
     * @param id 
     * @param x 
     * @param y 
     * @param time 
     * @param velocity 
     */
    explicit Berth(int id = INVALID_BERTH_ID, int x = -1, int y = -1, int time = -1, int velocity = -1):
        Block(x, y, BERTH_X_WIDTH, BERTH_Y_WIDTH) {
        set(id, x, y, time, velocity);
    }
    /**
     * @brief 获取区间内的货物总价值
     * @param nums 
     * @return int 
     */
    int getValue(int start, int end) const {
        if (start < 0 || end < 0 || start >= end || start >= goods_list.size()) {
            return 0;
        }
        if (end > goods_list.size()) {
            end = goods_list.size();
        }
        int value = 0;
        for (int i = start; i < end; ++i) {
            value += goods_list[i]->value;
        }
        return value;
    }
    /**
     * @brief 获取前nums个货物总价值
     * @param nums 前nums个货物
     */
    int getValue(int nums) const {
        return getValue(0, nums);
    }
    /**
     * @brief 获取货物总价值
     */
    int getValue() const {
        return getValue(0, goods_list.size());
    }
    /**
     * @brief 获取当前已放置货物总数
     * @return int 
     */
    int getGoodsNum() const {
        return goods_list.size();
    }
    /**
     * @brief 禁用泊位
     */
    void lock() {
        locked = true;
    }
    /**
     * @brief 泊位是否被禁用
     * @return true 
     * @return false 
     */
    bool isLocked() const {
        return locked;
    }
};

#endif // __BERTH_HPP__