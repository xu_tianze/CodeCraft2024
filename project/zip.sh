#!/bin/bash

SCRIPT=$(readlink -f "$0")
BASEDIR=$(dirname "$SCRIPT")
cd $BASEDIR

if [ ! -f CMakeLists.txt ]
then
    echo "ERROR: $BASEDIR is not a valid CMake file of SDK_C++ for CodeCraft-2023."
    echo "  Please run this script in a regular directory of SDK_C++."
    exit -1
fi

if [ x$1 != x ]
then
    rm -f ../submits/$1.zip
    zip -9jrq ../submits/$1.zip *.cpp *.cc *.c *.hpp *.h backups/CMakeLists.txt
else
    rm -f ../submits/submit"+%Y-%m-%d %H:%M:%S".zip
    zip -9jr ../submits/submit"+%Y-%m-%d %H:%M:%S".zip *.cpp *.cc *.c *.hpp *.h backups/CMakeLists.txt
fi
