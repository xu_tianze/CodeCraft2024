#ifndef __PLAN_FOR_SEA_HPP__
#define __PLAN_FOR_SEA_HPP__

#include "globaldef.hpp"
#include "Plan.hpp"

/**
 * @brief 海洋任务方案
 */
class PlanForSea : public Plan
{
public:
    int leaveTime = 0; // 离港时间（帧数大于该帧时离开泊位）
    bool autoSell = true; // 任务结束后自动前往虚拟点卸货，卸货后才算作方案完成

public:
    /**
     * @brief 默认构造函数
     */
    PlanForSea() = default;
    /**
     * @brief 初始化方案并设为有效
     * @param berthID 
     * @param leaveTime 
     * @param autoSell 方案完成后是否自动前往虚拟点卸货
     */
    void set(int berthID, int leaveTime, bool autoSell = true) {
        this->berthID = berthID;
        this->leaveTime = leaveTime;
        this->autoSell = autoSell;
        this->setValidate(true);
    }
    /**
     * @brief 构造函数
     * @param goods 
     * @param berthID 
     * @param autoSell 方案完成后是否自动前往虚拟点卸货
     */
    PlanForSea(int berthID, int leaveTime, bool autoSell = true) {
        set(berthID, leaveTime, autoSell);
    }
};

#endif // __PLAN_FOR_SEA_HPP__