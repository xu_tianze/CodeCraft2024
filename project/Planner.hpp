#ifndef __PLANNER_HPP__
#define __PLANNER_HPP__

#include "globaldef.hpp"
#include "PlanForLand.hpp"
#include "PlanForSea.hpp"
#include "Map.hpp"
#include "Berth.hpp"
#include "Goods.hpp"
#include "Robot.hpp"
#include "Boat.hpp"
#include "RobotDispatcher.hpp"
#include "BoatDispatcher.hpp"
#include <vector>

/**
 * @brief 规划器
 */
class Planner {
public:
    /**
     * @brief 析构函数
     */
    virtual ~Planner() {}
    /**
     * @brief 初始化
     * @param map 地图
     * @param berths 泊位列表
     * @param goods_list 货物列表
     * @param boatDispatchers 船调度器列表
     * @param robotDispatchers 机器人调度器列表
     * @note 初始化阶段调用一次，连接到可用的地图和泊位、货物列表等，由外部保证这些数据实时刷新
     */
    virtual void init(const Map *map, std::vector<Berth> *berths, const std::vector<Goods *> *goods_list,
        const std::vector<BoatDispatcher> *boatDispatchers, const std::vector<RobotDispatcher> *robotDispatchers) = 0;
    /**
     * @brief 刷新一帧
     * @param frameID 当前帧
     * @param newGoodsNum 新增货物数
     * @note  每帧运行一次（可能存在跳帧），新增货物数用于倒序遍历goods_list
     */
    virtual void refresh(int frameID, int newGoodsNum) = 0;
    /**
     * @brief 给指定机器人分配任务
     * @param robot 指定机器人
     * @return PlanForLand 分配的任务
     */
    virtual PlanForLand allocatePlan(const Robot *robot) = 0;
    /**
     * @brief 给指定船分配任务
     * @param boat 指定船
     * @return PlanForSea 分配的任务
     */
    virtual PlanForSea allocatePlan(const Boat *boat) = 0;
};

#endif // __PLANNER_HPP__