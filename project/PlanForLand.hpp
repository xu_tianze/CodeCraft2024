#ifndef __PLAN_FOR_LAND_HPP__
#define __PLAN_FOR_LAND_HPP__

#include "globaldef.hpp"
#include "Plan.hpp"
#include "Goods.hpp"

/**
 * @brief 陆地任务方案
 */
class PlanForLand : public Plan
{
public:
    Goods *goods = nullptr; // 货物
    int berthArea = INVALID_BERTH_ID; // 方案运行的港口辖区（仅在采用货物与泊位辖区绑定策略时才使用）

public:
    /**
     * @brief 默认构造函数
     */
    PlanForLand() = default;
    /**
     * @brief 初始化方案并设为有效
     * @param goods 
     * @param berthID 
     * @param value 
     */
    void set(Goods *goods, int berthID, float value = 0.0) {
        this->goods = goods;
        this->value = value;
        this->berthID = berthID;
        this->setValidate(true);
    }
    /**
     * @brief 设置港口辖区（仅在采用货物与泊位辖区绑定策略时才使用）
     * @param berthArea 
     */
    void setBerthArea(int berthArea) {
        this->berthArea = berthArea;
    }
    /**
     * @brief 构造函数
     * @param goods 
     * @param berthID 
     * @param value 
     */
    PlanForLand(Goods *goods, int berthID, float value = 0.0) {
        set(goods, berthID, value);
    }
};

#endif // __PLAN_FOR_LAND_HPP__