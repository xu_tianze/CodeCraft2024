#ifndef __ROBOT_HPP__
#define __ROBOT_HPP__

#include <cstdio>
#include "globaldef.hpp"
#include "Block.hpp"

/**
 * @brief 机器人
 */
class Robot: public Block
{
public:
    int id; // 编号
    bool carried = false; // 是否装载了货物
    bool running = true; // 是否为正常运行状态

public:
    /**
     * @brief 初始化
     * @param id
     * @param x
     * @param y
     */
    void set(int id, int x = -1, int y = -1) {
        this->id = id;
        this->x = x;
        this->y = y;
    }
    /**
     * @brief 构造函数
     * @param id
     * @param x
     * @param y
     */
    explicit Robot(int id = -1, int x = -1, int y = -1):
        Block(x, y, ROBOT_X_WIDTH, ROBOT_Y_WIDTH),
        id(id) {

    }
    /**
     * @brief 刷新状态
     * @param carried
     * @param x
     * @param y
     * @param running
     */
    void refresh(int carried, int x, int y, int running) {
        this->carried = static_cast<bool>(carried);
        setCoord(x, y);
        this->running = static_cast<bool>(running);
    }
    /**
     * @brief 装载货物
     * @param goods
     */
    void load() {
        printf("get %d\n", id);
        std::fflush(stdout); // 标准输出需要及时刷新
    }
    /**
     * @brief 卸货
     */
    void unload() {
        printf("pull %d\n", id);
        std::fflush(stdout); // 标准输出需要及时刷新
    }
    /**
     * @brief 向指定方向移动一格
     * @param dir 
     */
    void move(direction_t dir) {
        if (dir != INVALID && dir != INPLACE) {
            printf("move %d ", id);
            switch (dir)
            {
            case UP:
                printf("2");
                break;
            case RIGHT:
                printf("0");
                break;
            case DOWN:
                printf("3");
                break;
            case LEFT:
                printf("1");
                break;
            default:
                break;
            }
            printf("\n");
            std::fflush(stdout); // 标准输出需要及时刷新
        }
    }
};

#endif // __ROBOT_HPP__