#ifndef __GLOBALDEF_HPP__
#define __GLOBALDEF_HPP__

#include "log.hpp"
#include "Hyperparameters.hpp"

const int MAP_X_MAX = 200; // 地图宽度
const int MAP_Y_MAX = 200; // 地图高度

const char MARK_LAND = '.'; // 空地
const char MARK_OCEAN = '*'; // 海洋
const char MARK_OBSTACLE = '#'; // 障碍
const char MARK_ROBOT = 'A'; // 机器人
const char MARK_BERTH = 'B'; // 泊位

const unsigned short DISTANCE_INF = 0XFFFF; // 无限距离

/**
 * @brief 方向
 */
typedef enum {
    INVALID = -1, // 无效
    INPLACE = 0, // 原地
    UP = 1, // 上
    RIGHT = 2, // 右
    DOWN = 3, // 下
    LEFT = 4, // 左
} direction_t;

const int BERTH_NUM = 10; // 泊位数量
const int BERTH_X_WIDTH = 4; // 泊位横向长度
const int BERTH_Y_WIDTH = 4; // 泊位纵向长度
const int VIRTUAL_BERTH_ID = -1; // 虚拟点ID
const int INVALID_BERTH_ID = -2; // 无效泊位ID
const int BERTH_INTER_DIS = 500; // 泊位间距离

const int BOAT_NUM = 5; // 船数量
const int BOAT_X_WIDTH = 2; // 船横向长度
const int BOAT_Y_WIDTH = 4; // 船纵向长度

const int ROBOT_NUM = 10; // 机器人数量
const int ROBOT_X_WIDTH = 1; // 机器人横向长度
const int ROBOT_Y_WIDTH = 1; // 机器人纵向长度

const int TIME_FRAME_MAX = 15000; // 总帧数

const int GOODS_LIFE_FRAMES = 1000; // 货物存留时长

const int BOAT_MOVING = 0; // 船在移动（运输）中
const int BOAT_LOADING = 1; // 船在装货状态或运输完成状态
const int BOAT_WAITING = 2; // 船在泊位外等待

Hyperparameters PARAMS; // 超参数

/**
 * @brief 打印距离地图
 * @param map 
 * @param disMap 
 */
#ifdef USE_LOG
#define LOG_PRINT_DISTANCE_MAP(map, disMap, invalid_value) { \
    for (int x = 0; x < MAP_X_MAX; ++x) { \
        for (int y = 0; y < MAP_Y_MAX; ++y) { \
            if ((disMap)[x][y] == (invalid_value)) { \
                LOG(" %c  ", (map)[x][y]); \
            } else { \
                LOG("%03d ", (disMap)[x][y]); \
            } \
        } \
        LOG("\n"); \
    } \
}
#else
#define LOG_PRINT_DISTANCE_MAP(...)
#endif // USE_LOG

#endif // __GLOBALDEF_HPP__