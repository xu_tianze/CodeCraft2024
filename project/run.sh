#!/bin/bash

rm -f log/* replay/*

echo "map1"
../sdk/PreliminaryJudge -m maps/map1.txt  -r map1.rep -l NONE -s 123 "./main log/log_map1.txt"

echo "map2"
../sdk/PreliminaryJudge -m maps/map2.txt  -r map2.rep -l NONE -s 123 "./main log/log_map2.txt"

echo "map3"
../sdk/PreliminaryJudge -m maps/map3.txt  -r map3.rep -l NONE -s 123 "./main log/log_map3.txt"

echo "map4"
../sdk/PreliminaryJudge -m maps/map4.txt  -r map4.rep -l NONE -s 123 "./main log/log_map4.txt"

echo "map5"
../sdk/PreliminaryJudge -m maps/map5.txt  -r map5.rep -l NONE -s 123 "./main log/log_map5.txt"

echo "map6"
../sdk/PreliminaryJudge -m maps/map6.txt  -r map6.rep -l NONE -s 123 "./main log/log_map6.txt"

echo "map7"
../sdk/PreliminaryJudge -m maps/map7.txt  -r map7.rep -l NONE -s 123 "./main log/log_map7.txt"

echo "map8"
../sdk/PreliminaryJudge -m maps/map8.txt  -r map8.rep -l NONE -s 123 "./main log/log_map8.txt"

# echo "PleasePartitionThisMap"
# ../sdk/PreliminaryJudge -m maps/PleasePartitionThisMap.txt  -r PleasePartitionThisMap.rep -l NONE -s 123 "./main log/log_PleasePartitionThisMap.txt"

# echo my1
# ../sdk/PreliminaryJudge -m maps/avoidance_master.txt -r avoidance_master.rep -l NONE -s 123 "./main log/log_avoidance_master.txt"