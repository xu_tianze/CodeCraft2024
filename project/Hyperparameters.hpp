#ifndef __HYPERPARAMETERS_HPP__
#define __HYPERPARAMETERS_HPP__

#include "globaldef.hpp"
#include <vector>
#include <utility>

/**
 * @brief 超参数
 */
class Hyperparameters {
// 需要人工设置的超参数
public: 
    // 地图编号
    int mapID = -1;

    // 使用的规划器
    enum {
        Random,
        BindRobot,
        PreQue,
        WorkGroup,
    } Planner = WorkGroup;

    // 针对地图的特殊参数
    std::vector<int> LOCK_BERTHS = {}; // 锁定的泊位

    // 机器人调度器相关
    unsigned int PLAN_STEPS = 20; // 寻路时预判的步数

    // 任务规划器相关
    bool ALLOW_CROSS_AREA_GET_GOODS = true; // 允许跨区取货
    int FREE_AWAY_DISTANCE = 1; // 处于空闲状态时，退到泊位外多远的距离
    int ROBOT_MOVE_MARGIN_TIME = 0; // 机器人运行时因碰撞等可能耽误的时间
    int GOODS_LIFE_FACTOR = 250; // 货物快消失时的价值补偿
    float ROUND_APPEND_FACTOR = 1; // 机器人本轮来不及送货，需要延迟到下一轮时的惩罚时间系数（轮港频率越高，该值应越低，值域为0~1）

    // 仅采用工作组策略时有用
    std::vector<std::pair<std::vector<int>, std::vector<int>>> WORK_GROUPS; // 前面是泊位，后面是机器人

    void init(int mapID) {
        switch(mapID) {
        case 1:
            // autoset1
            PLAN_STEPS = 26;
            GOODS_LIFE_FACTOR = 134;
            ROUND_APPEND_FACTOR = 0.9;
            FREE_AWAY_DISTANCE = 7;
            ROBOT_MOVE_MARGIN_TIME = 18;
            // end autoset1
            break;
        case 2:
            // autoset2
            PLAN_STEPS = 37;
            GOODS_LIFE_FACTOR = 108;
            ROUND_APPEND_FACTOR = 0.5;
            FREE_AWAY_DISTANCE = 0;
            ROBOT_MOVE_MARGIN_TIME = 11;
            // end autoset2
            break;
        case 3:
            // autoset3
            PLAN_STEPS = 37;
            GOODS_LIFE_FACTOR = 392;
            ROUND_APPEND_FACTOR = 1.0;
            FREE_AWAY_DISTANCE = 2;
            ROBOT_MOVE_MARGIN_TIME = 3;
            // end autoset3
            break;
        case 4:
            WORK_GROUPS = {
                std::make_pair(std::vector<int>{0,1}, std::vector<int>{4}),
                std::make_pair(std::vector<int>{2}, std::vector<int>{0}),
                std::make_pair(std::vector<int>{3}, std::vector<int>{1}),
                std::make_pair(std::vector<int>{4,5}, std::vector<int>{2,5,8}),
                std::make_pair(std::vector<int>{6}, std::vector<int>{6}),
                std::make_pair(std::vector<int>{7}, std::vector<int>{7}),
                std::make_pair(std::vector<int>{8,9}, std::vector<int>{9}),
            };
            ALLOW_CROSS_AREA_GET_GOODS = false;
            // autoset4
            PLAN_STEPS = 17;
            GOODS_LIFE_FACTOR = 361;
            ROUND_APPEND_FACTOR = 0.5;
            FREE_AWAY_DISTANCE = 10;
            ROBOT_MOVE_MARGIN_TIME = 2;
            // end autoset4
            break;
        case 5:
            // autoset5
            PLAN_STEPS = 30;
            GOODS_LIFE_FACTOR = 165;
            ROUND_APPEND_FACTOR = 0.8;
            FREE_AWAY_DISTANCE = 4;
            ROBOT_MOVE_MARGIN_TIME = 17;
            // end autoset5
            break;
        case 6:
            // autoset6
            PLAN_STEPS = 21;
            GOODS_LIFE_FACTOR = 132;
            ROUND_APPEND_FACTOR = 0.5;
            FREE_AWAY_DISTANCE = 8;
            ROBOT_MOVE_MARGIN_TIME = 11;
            // end autoset6
            break;
        case 7:
            // autoset7
            PLAN_STEPS = 21;
            GOODS_LIFE_FACTOR = 276;
            ROUND_APPEND_FACTOR = 0.5;
            FREE_AWAY_DISTANCE = 1;
            ROBOT_MOVE_MARGIN_TIME = 2;
            // end autoset7
            break;
        case 8:
            // autoset8
            PLAN_STEPS = 13;
            GOODS_LIFE_FACTOR = 204;
            ROUND_APPEND_FACTOR = 0.7;
            FREE_AWAY_DISTANCE = 8;
            ROBOT_MOVE_MARGIN_TIME = 6;
            // end autoset8
            break;
        default:
            break;
        }
    }
};

#endif // __HYPERPARAMETERS_HPP__
