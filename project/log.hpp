#ifndef __LOG_HPP__
#define __LOG_HPP__

#ifdef USE_LOG
#include <cstdio>
#define LOG_INIT(LOG_FILE_PATH) {std::freopen(LOG_FILE_PATH, "w+", stderr);}
#define LOG(arg...) {std::fprintf(stderr, ##arg); std::fflush(stderr);}
#define LOG_POINT(NUM) LOG("[%d]###############################################################\n", NUM)
#else
#define LOG_INIT(...)
#define LOG(...)
#define LOG_POINT(...)
#endif

#endif // __LOG_HPP__